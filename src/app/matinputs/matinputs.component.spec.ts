import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatinputsComponent } from './matinputs.component';

describe('MatinputsComponent', () => {
  let component: MatinputsComponent;
  let fixture: ComponentFixture<MatinputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatinputsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatinputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
