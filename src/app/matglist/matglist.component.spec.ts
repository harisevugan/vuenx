import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatglistComponent } from './matglist.component';

describe('MatglistComponent', () => {
  let component: MatglistComponent;
  let fixture: ComponentFixture<MatglistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatglistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatglistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
