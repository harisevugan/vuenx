import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material/material.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatbasketComponent } from './matbasket/matbasket.component';
import { MattbComponent } from './mattb/mattb.component';
import { MatsnComponent } from './matsn/matsn.component';
import { MatglistComponent } from './matglist/matglist.component';
import { MatexppanelandcardsComponent } from './matexppanelandcards/matexppanelandcards.component';
import { MatinputsComponent } from './matinputs/matinputs.component';

@NgModule({
  declarations: [
    AppComponent,
    MatbasketComponent,
    MattbComponent,
    MatsnComponent,
    MatglistComponent,
    MatexppanelandcardsComponent,
    MatinputsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
