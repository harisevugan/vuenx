import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MatbasketComponent } from './matbasket/matbasket.component';
import { MattbComponent } from './mattb/mattb.component';
import {MatsnComponent} from './matsn/matsn.component';
import {MatglistComponent} from './matglist/matglist.component';
import {MatexppanelandcardsComponent} from './matexppanelandcards/matexppanelandcards.component'
import {MatinputsComponent} from './matinputs/matinputs.component';

const routes: Routes = [
  {
    path: 'material/basket',
    component: MatbasketComponent
  },
  {
    path: 'material/topnav',
    component: MattbComponent
  },
  {
    path: 'material/sidenav',
    component: MatsnComponent
  },
  {
    path: 'material/gridlist',
    component: MatglistComponent
  },
  {
    path: 'material/exppc',
    component: MatexppanelandcardsComponent
  },
  {
    path: 'material/input',
    component: MatinputsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
