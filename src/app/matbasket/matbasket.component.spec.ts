import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatbasketComponent } from './matbasket.component';

describe('MatbasketComponent', () => {
  let component: MatbasketComponent;
  let fixture: ComponentFixture<MatbasketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatbasketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatbasketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
