import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatsnComponent } from './matsn.component';

describe('MatsnComponent', () => {
  let component: MatsnComponent;
  let fixture: ComponentFixture<MatsnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatsnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatsnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
