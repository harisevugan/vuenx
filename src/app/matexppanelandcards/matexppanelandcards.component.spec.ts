import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatexppanelandcardsComponent } from './matexppanelandcards.component';

describe('MatexppanelandcardsComponent', () => {
  let component: MatexppanelandcardsComponent;
  let fixture: ComponentFixture<MatexppanelandcardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatexppanelandcardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatexppanelandcardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
